const players = [
  "Will",
  "MadDawg",
  "Tommy",
  "Stuart",
  "POB",
  "Oz",
  "Dave",
  "Jake",
  "Mike",
  "JMurph",
  "Tim",
  "Matt",
  "Ross",
];

// used to shuffle an array in place
const randomSort = (a, b) => (Math.random() < 0.5 ? -1 : 1);

players.sort(randomSort);

const allPlayersTable = document.querySelector("#players");
players.forEach((name) => {
  const playerRow = allPlayersTable.appendChild(document.createElement("tr"));
  playerRow.id = `row-${name}`;
  const playerNameCell = playerRow.appendChild(document.createElement("th"));
  playerNameCell.textContent = name;
});

const potA = [
  "Argentina",
  "Brazil",
  "France",
  "Spain",
  "England",
  "Germany",
  "Belgium",
  "Netherlands",
];

const potB = [
  "Denmark",
  "Croatia",
  "Portugal",
  "Senegal",
  "Uruguay",
  "Poland",
  "Mexico",
  "Switzerland",
];

const potC = [
  "Japan",
  "Serbia",
  "South Korea",
  "Wales",
  "Ecuador",
  "Costa Rica",
  "Canada",
  "USA",
];
const potD = [
  "Iran",
  "Morocco",
  "Cameroon",
  "Australia",
  "Qatar",
  "Saudi Arabia",
  "Ghana",
  "Tunisia",
];

const allCountries = [...potA, ...potB, ...potC, ...potD];

const potARow = document.querySelector("#potA");
potA.forEach((country) => {
  const cell = document.createElement("td");
  const countryName = document.createElement("span");
  countryName.textContent = country;

  const countryCount = document.createElement("span");
  countryCount.id = `count-${country.replace(" ", "-")}`;
  cell.appendChild(countryName);
  cell.appendChild(countryCount);
  potARow.appendChild(cell);
});

const potBRow = document.querySelector("#potB");
potB.forEach((country) => {
  const cell = document.createElement("td");
  const countryName = document.createElement("span");
  countryName.textContent = country;

  const countryCount = document.createElement("span");
  countryCount.id = `count-${country.replace(" ", "-")}`;
  cell.appendChild(countryName);
  cell.appendChild(countryCount);
  potBRow.appendChild(cell);
});

const potCRow = document.querySelector("#potC");
potC.forEach((country) => {
  const cell = document.createElement("td");
  const countryName = document.createElement("span");
  countryName.textContent = country;

  const countryCount = document.createElement("span");
  countryCount.id = `count-${country.replace(" ", "-")}`;
  cell.appendChild(countryName);
  cell.appendChild(countryCount);
  potCRow.appendChild(cell);
});

const potDRow = document.querySelector("#potD");
potD.forEach((country) => {
  const cell = document.createElement("td");
  const countryName = document.createElement("span");
  countryName.textContent = country;

  const countryCount = document.createElement("span");
  countryCount.id = `count-${country.replace(" ", "-")}`;
  cell.appendChild(countryName);
  cell.appendChild(countryCount);
  potDRow.appendChild(cell);
});

/**
 * Clone the pots - don't mutate original arrays
 */
const refreshAvailableCountries = () => [
  [...potA],
  [...potB],
  [...potC],
  [...potD],
];

const arraysDoOverlap = (array1, array2) => {
  const count = Math.min(array1.length, array2.length);
  let overlapCount = 0;
  for (let i = 0; i < count; i++) {
    if (array1[i] === array2[i]) {
      overlapCount++;
    }
  }

  return overlapCount > 1;
};

/**
 * @returns a map of each person to a random array of countries (1 from each pot)
 */
const chooseCountries = () => {
  /**
   * Map each person to an array of their countries
   */
  const countryMap = {};

  const playersLeft = [...players];

  let [a, b, c, d] = refreshAvailableCountries();
  a.sort(randomSort);
  b.sort(randomSort);
  c.sort(randomSort);
  d.sort(randomSort);

  while (playersLeft.length > 0) {
    if (a.length === 0) {
      [a, b, c, d] = refreshAvailableCountries();
      a.sort(randomSort);
      b.sort(randomSort);
      c.sort(randomSort);
      d.sort(randomSort);
    }

    const nextPlayer = playersLeft.shift();

    let countries = [a.pop(), b.pop(), c.pop(), d.pop()];

    /**
     * make sure that we don't have a person sharing >1 country with another.
     * shuffle again if that is the case
     */
    const existingChoices = Object.values(countryMap);
    while (existingChoices.some((c) => arraysDoOverlap(c, countries))) {
      console.log("CLASH!", existingChoices, countries);
      a.push(countries[0]);
      b.push(countries[1]);
      c.push(countries[2]);
      d.push(countries[3]);

      a.sort(randomSort);
      b.sort(randomSort);
      c.sort(randomSort);
      d.sort(randomSort);

      countries = [a.pop(), b.pop(), c.pop(), d.pop()];
    }

    countryMap[nextPlayer] = countries;
  }

  return countryMap;
};

const sleep = async (ms) => {
  return new Promise((resolve) => setTimeout(resolve, ms ?? 1));
};

const populateTable = async (_interval) => {
  const interval = _interval ?? 0;
  // clear any existing choices
  document.querySelectorAll("table#players td").forEach((el) => el.remove());

  // generate new random choices
  const choices = chooseCountries();

  const populateCounts = () => {
    allCountries.forEach((country) => {
      const countElement = document.querySelector(
        `#count-${country.replace(" ", "-")}`
      );
      const count = Array.from(document.querySelectorAll("#players td")).filter(
        (el) => el.textContent === country
      ).length;
      countElement.textContent = `(${count})`;
    });
  };

  // populate the table
  for (let i = 0; i < 4; i++) {
    for (person of players) {
      const countries = choices[person];
      _interval && (await sleep(interval));
      const playerRow = document.querySelector(`#row-${person}`);
      const countryCell = playerRow.appendChild(document.createElement("td"));
      countryCell.textContent = countries[i];
      populateCounts();
    }
  }
};
